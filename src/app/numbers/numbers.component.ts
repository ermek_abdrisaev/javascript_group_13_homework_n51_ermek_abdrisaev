import {Component} from "@angular/core";

@Component({
  selector: 'app-numbers',
  templateUrl: './numbers.component.html',
  styleUrls: ['./numbers.component.css']
})

export class NumbersComponent {
  numbers: number[] = [];

  constructor(){
    this.getRandomNumber();

  }

  getRandomNumber(){
    for(let i = 0; i < 5; i++){
      let result = Math.floor(Math.random()*36) + 5;
      this.numbers.push(result);
      this.numbers.sort((a,b) => a - b)
      this.numbers.filter(result => 1)
    }
  };


  onNewNumbers(event: Event){
    event.preventDefault();
    this.getRandomNumber();
  }
}
