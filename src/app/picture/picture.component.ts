import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-picture',
  templateUrl: './picture.component.html',
  styleUrls: ['./picture.component.css']
})
export class PictureComponent {
  @Input() picName = 'pickName';
  @Input() icon = 'https://assets.pokemon.com/assets/cms2/img/pokedex/full/087.png'
}

