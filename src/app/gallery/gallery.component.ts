import {Component} from '@angular/core';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.css']
})
export class GalleryComponent {
  picName = "";
  password = "";
  icon = "";


  showForm = false;
  addImage = false;

  gallery = [
    {picName: "SeaLeon", icon: "https://assets.pokemon.com/assets/cms2/img/pokedex/full/087.png"},
    {picName: "Grookey", icon: "https://assets.pokemon.com/assets/cms2/img/pokedex/full/810.png"},
    {picName: "Poliwhirl", icon: "https://assets.pokemon.com/assets/cms2/img/pokedex/full/061.png"},

  ];

  onAddImage(event: Event){
    event.preventDefault();
    this.gallery.push({
      picName: this.picName,
      icon: this.icon,
    });
  }

  onPasswordInput (event: Event){
   const target = <HTMLInputElement>event.target;
   this.password = target.value
  };

  onImageTitleInput(event: Event){
    const target = <HTMLInputElement>event.target;
    this.icon = target.value
  }


  formIsEmpty() {
    return this.password === '';
  }
}
