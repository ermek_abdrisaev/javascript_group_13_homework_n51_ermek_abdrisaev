import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import {NumbersComponent} from "./numbers/numbers.component";
import { LotComponent } from './lot/lot.component';
import { PictureComponent } from './picture/picture.component';
import { GalleryComponent } from './gallery/gallery.component';
import {FormsModule} from "@angular/forms";

@NgModule({
  declarations: [
    AppComponent,
    LotComponent,
    NumbersComponent,
    GalleryComponent,
    PictureComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
